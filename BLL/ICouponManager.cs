﻿using DAL.Model;

namespace BLL
{
    public interface ICouponManager
    {
        void Apply(Booking booking, Coupon coupon);
    }
}