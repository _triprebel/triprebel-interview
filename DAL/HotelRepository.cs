﻿using DAL.Model;
using System;
using System.Collections.Generic;

namespace DAL
{
    /// <summary>
    ///  This stubs out the database - normally this would be actually connected
    ///  to some API
    /// </summary>
    public class HotelRepository : IHotelRepository
    {
        private static HotelRepository _instance;
        public static HotelRepository Instance { get
            {
                if(_instance == null)
                {
                    _instance = new HotelRepository();
                }
                return _instance;
            }
        }
        
        private IList<Hotel> Hotels { get; set; }

        private HotelRepository()
        {
            var hilton = new Hotel()
            {
                Id = 1,
                Name = "The Hilton",
                PricePerNight = 2300.0
            };

            var ritz = new Hotel()
            {
                Id = 2,
                Name = "The Ritz",
                PricePerNight = 1500.0
            };

            var jahreszeiten = new Hotel()
            {
                Id = 3,
                Name = "Vier Jahreszeiten",
                PricePerNight = 1800.0
            };

            Hotels = new List<Hotel>()
            {
                hilton, ritz, jahreszeiten
            };
        }

        public IEnumerable<Hotel> GetHotels(DateTime checkIn, DateTime checkOut, int numberOfGuests)
        {
            return Hotels;
        }
    }
}
