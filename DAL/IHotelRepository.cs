﻿using DAL.Model;
using System;
using System.Collections.Generic;

namespace DAL
{
    public interface IHotelRepository
    {
        IEnumerable<Hotel> GetHotels(DateTime checkIn, DateTime checkOut, int numberOfGuests);
    }
}