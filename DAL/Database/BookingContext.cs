﻿using DAL.Model;
using System.Data.Entity;

namespace DAL.Database
{
    public class BookingContext : DbContext
    {
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Coupon> Coupons { get; set; }
    }
}
